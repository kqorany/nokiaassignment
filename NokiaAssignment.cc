/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2010 IITP RAS
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Kirill Andreev <andreev@iitp.ru>
 * Author: Khaked Abdelfadeel <khaled.abdelfadeel@ieee.org>
 *
 * Nokia Bell Labs Assignment
 */

#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mesh-module.h"
#include "ns3/mobility-module.h"
#include "ns3/mesh-helper.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/wifi-helper.h"
#include <ns3/flow-monitor-helper.h>
#include "ns3/gnuplot.h"
#include "ns3/netanim-module.h"
#include "ns3/propagation-loss-model.h"
#include "ns3/propagation-delay-model.h"
#include "src/point-to-point/helper/point-to-point-helper.h"
#include "src/csma/helper/csma-helper.h"
#include "ns3/bulk-send-application.h"
#include "ns3/olsr-helper.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "src/network/model/packet-metadata.h"

#include "scratch/mesh-tcp.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <string>
#include <fstream>

 using namespace ns3;

 //A File parameter
 std::ofstream DataFile;

 NS_LOG_COMPONENT_DEFINE ("NokiaTest");
 void ThroughputMonitor (FlowMonitorHelper *fmhelper, Ptr<FlowMonitor> flowMon, Gnuplot2dDataset DataSet);

 //-----------------------------------FUNCTIONS FOR CREATING SOCKETS

void createTcpSocket(NodeContainer csink, Ipv4InterfaceContainer ifcontsink, NodeContainer csource, Ipv4InterfaceContainer ifcontsource, int sink, int source, int sinkPort, double startTime, double stopTime, uint32_t packetSize, uint32_t numPackets, std::string dataRate)
{

	Address sinkAddress1 (InetSocketAddress (ifcontsink.GetAddress (sink), sinkPort));
	PacketSinkHelper packetSinkHelper1 ("ns3::TcpSocketFactory", InetSocketAddress (ifcontsource.GetAddress(source), sinkPort));
	ApplicationContainer sinkApps1 = packetSinkHelper1.Install (csink.Get (sink));
	sinkApps1.Start (Seconds (0.));
	sinkApps1.Stop (Seconds (stopTime));

	Ptr<Socket> ns3TcpSocket1 = Socket::CreateSocket (csource.Get (source), TcpSocketFactory::GetTypeId ());
	Ptr<MyApp> app1 = CreateObject<MyApp> ();
	app1->Setup (ns3TcpSocket1, sinkAddress1, packetSize, numPackets, DataRate (dataRate));
	csource.Get (source)->AddApplication (app1);
	app1->SetStartTime (Seconds (startTime));
	app1->SetStopTime (Seconds (stopTime));

}

 class NokiaTest
 {
 public:
   /// Init test
   NokiaTest ();
   /// Configure test from command line arguments
   void Configure (int argc, char ** argv);
   /// Run test
   int Run ();

 private:
   double m_randomStart;
   double m_totalTime;
   double m_packetInterval;
   uint16_t m_packetSize;
   uint32_t m_nIfaces;
   std::string m_tcpudp;
   std::string m_downup;
   bool m_chan;
   bool m_pcap;
   std::string m_stack;
   std::string m_root;
   Ptr<FlowMonitor> flowMon;

   // NodeContainer for individual nodes
   NodeContainer nc_sta;  // WiFi Stations
   NodeContainer nc_mp;   // Mesh points
   NodeContainer nc_bb;   // Backbone

   // NodeContainer for categorical nodes
   //NodeContainer nc_mesh;

   // NodeContainer for connected nodes
   NodeContainer nc_gwBb;
   NodeContainer nc_mpAp;

   // List of p2p NetDevice Container
   NetDeviceContainer de_p2p_gwBb;

   // List of mesh NetDevice Container
   //NetDeviceContainer de_mesh;
   NetDeviceContainer de_sta;
   NetDeviceContainer de_mp;

   // List of categorical NetDevice Container
   NetDeviceContainer de_wifi_sta;
   NetDeviceContainer de_wifi_ap;
   NetDeviceContainer de_wifi_staAp;
   NetDeviceContainer de_csma_apMp;

   // List of interface container
   Ipv4InterfaceContainer if_mp;
   Ipv4InterfaceContainer if_wifi_sta;
   Ipv4InterfaceContainer if_wifi_ap;
   Ipv4InterfaceContainer if_csma_apMp;
   Ipv4InterfaceContainer if_p2p_gwBb;

   // Helpers
   MeshHelper mpHelper;
   PointToPointHelper p2pHelper;
   CsmaHelper csmaHelper;
   Ipv4AddressHelper address;

 private:
   /// Create nodes and setup their mobility
   void CreateNodes ();
   /// Install Internet m_stack on nodes
   void InstallInternetStack ();
   /// Install applications
   void InstallApplication ();
   /// Setup mobility
   void SetupMobility ();
   /// Create propagation loss
   void CreatePropagationLoss ();
   /// Print mesh devices diagnostics
   void Report ();
   /// Setup FlowMonitor
   void InstallFlowMonitor ();
 };

 NokiaTest::NokiaTest () :
   // Set up node numbers on the network.
   m_randomStart (0.1),
   m_totalTime (100.0),
   m_packetInterval (0.1),
   m_packetSize (1024),
   m_nIfaces (1),
   m_tcpudp ("udp"),
   m_downup ("up"),
   m_chan (true),
   m_pcap (false),
   m_stack ("ns3::Dot11sStack"),
   m_root ("ff:ff:ff:ff:ff:ff") { }

 void
 NokiaTest::Configure (int argc, char *argv[])
 {
   CommandLine cmd;
   cmd.AddValue ("Start", "Maximum random start delay, seconds. [0.1 s]", m_randomStart);
   cmd.AddValue ("Time", "Simulation time, seconds [100 s]", m_totalTime);
   cmd.AddValue ("TcpUdp", "Tcp [tcp] or Udp [udp]", m_tcpudp);
   cmd.AddValue ("DownUp", "Downlink [down] or Uplink [up]", m_downup);
   cmd.AddValue ("PacketSize", "Size of packets", m_packetSize);
   cmd.AddValue ("Interfaces", "Number of radio interfaces used by each mesh point. [1]", m_nIfaces);
   cmd.AddValue ("Channels", "Use different frequency channels for different interfaces. [0]", m_chan);
   cmd.AddValue ("Pcap", "Enable PCAP traces on interfaces. [0]", m_pcap);
   cmd.AddValue ("Stack", "Type of protocol stack. ns3::Dot11sStack by default", m_stack);
   cmd.AddValue ("Root", "Mac address of root mesh point in HWMP", m_root);

   cmd.Parse (argc, argv);
   NS_LOG_DEBUG ("Simulation time: " << m_totalTime << " s");
 }

 void
 NokiaTest::CreateNodes ()
 {
   //Create individual nodes in their node container
   nc_sta.Create (4);
   nc_mp.Create (3);
   nc_bb.Create (1);

   //Create connected nodes in their container
   nc_mpAp = NodeContainer (nc_mp.Get (2), nc_sta.Get (3));
   //nc_mesh = NodeContainer (nc_mp, nc_sta);
   nc_gwBb = NodeContainer(nc_bb, nc_mp.Get (0));


   // Create p2p links between backbone (bb) and mesh point 0 (nc_mp.Get(0))
   p2pHelper.SetDeviceAttribute ("DataRate", StringValue ("100Mbps"));
   p2pHelper.SetChannelAttribute ("Delay", TimeValue (NanoSeconds (6560)));
   de_p2p_gwBb = p2pHelper.Install (nc_gwBb);

   // Create CSMA connection between mesh point 2 (nc_mp.Get(2)) and Access Point (ap)
   csmaHelper.SetChannelAttribute ("DataRate", StringValue ("100Mbps"));
   csmaHelper.SetChannelAttribute ("Delay", TimeValue (NanoSeconds (6560)));
   de_csma_apMp = csmaHelper.Install (nc_mpAp);

   // Configure YansWifiChannel
   YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default ();
   YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default ();
   wifiPhy.SetChannel (wifiChannel.Create ());

   // configure the mesh points network and mesh sta network
   mpHelper = MeshHelper::Default ();
   if (!Mac48Address (m_root.c_str ()).IsBroadcast ())
   {
	   mpHelper.SetStackInstaller (m_stack, "Root", Mac48AddressValue (Mac48Address (m_root.c_str ())));
   }
   else
   {
	   //If root is not set, we do not use "Root" attribute, because it
	   //is specified only for 11s
	   mpHelper.SetStackInstaller (m_stack);
   }
   if (m_chan)
   {
	   mpHelper.SetSpreadInterfaceChannels (MeshHelper::SPREAD_CHANNELS);
   }
   else
   {
	   mpHelper.SetSpreadInterfaceChannels (MeshHelper::ZERO_CHANNEL);
   }
   mpHelper.SetMacType ("RandomStart", TimeValue (Seconds (m_randomStart)));
   // Set number of interfaces - default is single-interface mesh point
   mpHelper.SetNumberOfInterfaces (m_nIfaces);
   //de_mesh = mpHelper.Install (wifiPhy, nc_mesh);
   de_mp = mpHelper.Install (wifiPhy, nc_mp);
   de_sta = mpHelper.Install (wifiPhy, nc_sta);

   // pcap tracing
   if (m_pcap)
   {
	 wifiPhy.EnablePcapAll ("NokiaAssignment");
   }

   // Setup WiFi network
   WifiHelper wifi;// = WifiHelper::Default ();
   wifi.SetStandard (WIFI_PHY_STANDARD_80211b);
   wifi.SetRemoteStationManager ("ns3::AarfWifiManager");
   WifiMacHelper wifiMac;// = WifiMacHelper::Default ();
   // Station nodes are initialized for wifi network
   Ssid ssid = Ssid ("wifiNetwork");
   wifiMac.SetType ("ns3::StaWifiMac",
	                "Ssid", SsidValue (ssid),
	                "ActiveProbing", BooleanValue (true));
   de_wifi_sta = wifi.Install (wifiPhy, wifiMac, nc_sta);
   // Setup AP for wifi network
   wifiMac.SetType ("ns3::ApWifiMac",
	                "Ssid", SsidValue (ssid));
   de_wifi_ap.Add  (wifi.Install (wifiPhy, wifiMac, nc_sta.Get (3)));

   de_wifi_staAp.Add(de_wifi_sta);
   de_wifi_staAp.Add(de_wifi_ap);
 }

 void
 NokiaTest::SetupMobility ()
 {
   // Setup mobility for the mesh points
   MobilityHelper mpMobility;
   Ptr<ListPositionAllocator> mpPositionAlloc = CreateObject <ListPositionAllocator>();
   mpPositionAlloc ->Add (Vector (40, 40, 0));  // gateway
   mpPositionAlloc ->Add (Vector (80, 40, 0));  // mesh point
   mpPositionAlloc ->Add (Vector (120, 40, 0)); // mesh point
   mpMobility.SetPositionAllocator (mpPositionAlloc);
   mpMobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
   mpMobility.Install (nc_mp);

   // Setup mobility for the Backbone node
   MobilityHelper bbMobility;
   Ptr<ListPositionAllocator> bbPositionAlloc = CreateObject <ListPositionAllocator>();
   bbPositionAlloc ->Add (Vector (0, 40, 0)); // gateway
   bbMobility.SetPositionAllocator (bbPositionAlloc);
   bbMobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
   bbMobility.Install (nc_bb);

   // Setup mobility for the wifi stations
   MobilityHelper staMobility;
   staMobility.SetPositionAllocator ("ns3::RandomRectanglePositionAllocator",
	                                  "X", StringValue ("ns3::UniformRandomVariable[Min=0|Max=160]"),
	                                  "Y", StringValue ("ns3::UniformRandomVariable[Min=0|Max=80]"));
   staMobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
   staMobility.Install (nc_sta);
 }

 void
 NokiaTest::CreatePropagationLoss ()
 {
   // Create propagation loss matrix
   Ptr<MatrixPropagationLossModel> lossModel = CreateObject<MatrixPropagationLossModel> ();
   lossModel->SetDefaultLoss (200); // set default loss to 200 dB (no link)
   lossModel->SetLoss (nc_sta.Get (0)->GetObject<MobilityModel> (),nc_sta.Get (1)->GetObject<MobilityModel> (), 50); // set symmetric loss 0 <-> 1 to 50 dB
   lossModel->SetLoss (nc_sta.Get (0)->GetObject<MobilityModel> (),nc_sta.Get (2)->GetObject<MobilityModel> (), 50); // set symmetric loss 0 <-> 2 to 50 dB
   lossModel->SetLoss (nc_sta.Get (0)->GetObject<MobilityModel> (),nc_sta.Get (3)->GetObject<MobilityModel> (), 50); // set symmetric loss 0 <-> 3 to 50 dB

   lossModel->SetLoss (nc_sta.Get (1)->GetObject<MobilityModel> (),nc_sta.Get (2)->GetObject<MobilityModel> (), 50); // set symmetric loss 1 <-> 2 to 50 dB
   lossModel->SetLoss (nc_sta.Get (1)->GetObject<MobilityModel> (),nc_sta.Get (3)->GetObject<MobilityModel> (), 50); // set symmetric loss 1 <-> 3 to 50 dB

   lossModel->SetLoss (nc_sta.Get (2)->GetObject<MobilityModel> (),nc_sta.Get (3)->GetObject<MobilityModel> (), 50); // set symmetric loss 2 <-> 3 to 50 dB
 }

 void
 NokiaTest::InstallInternetStack ()
 {
   InternetStackHelper internetStackHelper;
   OlsrHelper routingProtocol;
   internetStackHelper.SetRoutingHelper (routingProtocol);

   // Setup Internet stack on the nodes
   internetStackHelper.Install (nc_sta);
   internetStackHelper.Install (nc_mp);
   internetStackHelper.Install (nc_bb);

   address.SetBase ("10.1.1.0", "255.255.255.0");
   if_wifi_sta = address.Assign (de_wifi_staAp);

   address.SetBase ("10.1.2.0", "255.255.255.0");
   if_csma_apMp = address.Assign (de_csma_apMp);

   address.SetBase ("20.1.1.0", "255.255.255.0");
   if_mp = address.Assign (de_mp);

   address.SetBase ("20.1.2.0", "255.255.255.0");
   if_p2p_gwBb = address.Assign (de_p2p_gwBb);
 }

 void
 NokiaTest::InstallApplication ()
 {
   // install CBR streams to saturate the channels
   ApplicationContainer cbrApps;
   uint16_t cbrPort = 12345;
   OnOffHelper onOffHelper ("ns3::UdpSocketFactory", InetSocketAddress (if_wifi_sta.GetAddress (3), cbrPort));
   onOffHelper.SetAttribute ("PacketSize", UintegerValue (500));
   onOffHelper.SetAttribute ("OnTime",  StringValue ("ns3::ConstantRandomVariable[Constant=1]"));
   onOffHelper.SetAttribute ("OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0]"));

   onOffHelper.SetAttribute ("DataRate", StringValue ("1000000bps"));
   onOffHelper.SetAttribute ("StartTime", TimeValue (Seconds (1.000000)));
   cbrApps.Add (onOffHelper.Install (nc_sta.Get (0)));

   onOffHelper.SetAttribute ("DataRate", StringValue ("1001100bps"));
   onOffHelper.SetAttribute ("StartTime", TimeValue (Seconds (1.001)));
   cbrApps.Add (onOffHelper.Install (nc_sta.Get (1)));

   onOffHelper.SetAttribute ("DataRate", StringValue ("1002200bps"));
   onOffHelper.SetAttribute ("StartTime", TimeValue (Seconds (1.002)));
   cbrApps.Add (onOffHelper.Install (nc_sta.Get (2)));

   if (m_tcpudp == "tcp")
   {
	   if (m_downup == "down")
     {
       createTcpSocket(nc_sta, if_wifi_sta, nc_gwBb, if_p2p_gwBb, 0,0, 8080, 1.0, m_totalTime, m_packetSize, 100000, "250kbps");
       createTcpSocket(nc_sta, if_wifi_sta, nc_gwBb, if_p2p_gwBb, 1,0, 8081, 1.0, m_totalTime, m_packetSize, 100000, "250kbps");
       createTcpSocket(nc_sta, if_wifi_sta, nc_gwBb, if_p2p_gwBb, 2,0, 8082, 1.0, m_totalTime, m_packetSize, 100000, "250kbps");
       createTcpSocket(nc_sta, if_wifi_sta, nc_gwBb, if_p2p_gwBb, 3,0, 8083, 1.0, m_totalTime, m_packetSize, 100000, "250kbps");
	   }
	   else if (m_downup == "up"){
       createTcpSocket(nc_gwBb, if_p2p_gwBb, nc_sta, if_wifi_sta, 0,0, 8080, 1.0, m_totalTime, m_packetSize, 100000, "5Mbps");
       createTcpSocket(nc_gwBb, if_p2p_gwBb, nc_sta, if_wifi_sta, 0,1, 8080, 1.0, m_totalTime, m_packetSize, 100000, "5Mbps");
       createTcpSocket(nc_gwBb, if_p2p_gwBb, nc_sta, if_wifi_sta, 0,2, 8080, 1.0, m_totalTime, m_packetSize, 100000, "5Mbps");
       createTcpSocket(nc_gwBb, if_p2p_gwBb, nc_sta, if_wifi_sta, 0,3, 8080, 1.0, m_totalTime, m_packetSize, 100000, "5Mbps");
	   }
   }
   else if (m_tcpudp == "udp")
   {
	   LogComponentEnable ("UdpEchoClientApplication", LOG_LEVEL_INFO);
	   LogComponentEnable ("UdpEchoServerApplication", LOG_LEVEL_INFO);
	   if (m_downup == "down"){
		   // Server is set on all STAs
		   UdpServerHelper udpServer (9);
		   ApplicationContainer udpServerApps = udpServer.Install (nc_sta);
		   udpServerApps.Start (Seconds (0.0));
		   udpServerApps.Stop (Seconds (m_totalTime));

		   // client is set on the backbone node
		   UdpClientHelper udpClient0 (if_wifi_sta.GetAddress  (0), 9);
		   UdpClientHelper udpClient1 (if_wifi_sta.GetAddress  (1), 9);
		   UdpClientHelper udpClient2 (if_wifi_sta.GetAddress  (2), 9);
		   UdpClientHelper udpClient3 (if_wifi_sta.GetAddress  (3), 9);

		   udpClient0.SetAttribute ("MaxPackets", UintegerValue ((uint32_t) (m_totalTime * (1 / m_packetInterval))));
		   udpClient1.SetAttribute ("MaxPackets", UintegerValue ((uint32_t) (m_totalTime * (1 / m_packetInterval))));
		   udpClient2.SetAttribute ("MaxPackets", UintegerValue ((uint32_t) (m_totalTime * (1 / m_packetInterval))));
		   udpClient3.SetAttribute ("MaxPackets", UintegerValue ((uint32_t) (m_totalTime * (1 / m_packetInterval))));

		   udpClient0.SetAttribute ("Interval", TimeValue (Seconds (m_packetInterval)));
		   udpClient1.SetAttribute ("Interval", TimeValue (Seconds (m_packetInterval)));
		   udpClient2.SetAttribute ("Interval", TimeValue (Seconds (m_packetInterval)));
		   udpClient3.SetAttribute ("Interval", TimeValue (Seconds (m_packetInterval)));

		   udpClient0.SetAttribute ("PacketSize", UintegerValue (m_packetSize));
		   udpClient1.SetAttribute ("PacketSize", UintegerValue (m_packetSize));
		   udpClient2.SetAttribute ("PacketSize", UintegerValue (m_packetSize));
		   udpClient3.SetAttribute ("PacketSize", UintegerValue (m_packetSize));

		   ApplicationContainer udpClientApp0 = udpClient0.Install (nc_bb);
		   ApplicationContainer udpClientApp1 = udpClient1.Install (nc_bb);
		   ApplicationContainer udpClientApp2 = udpClient2.Install (nc_bb);
		   ApplicationContainer udpClientApp3 = udpClient3.Install (nc_bb);

		   udpClientApp0.Start (Seconds (m_randomStart));
		   udpClientApp1.Start (Seconds (m_randomStart+0.12));
		   udpClientApp2.Start (Seconds (m_randomStart+0.13));
		   udpClientApp3.Start (Seconds (m_randomStart+0.14));

		   udpClientApp0.Stop (Seconds (m_totalTime));
		   udpClientApp1.Stop (Seconds (m_totalTime));
		   udpClientApp2.Stop (Seconds (m_totalTime));
		   udpClientApp3.Stop (Seconds (m_totalTime));
	   }
	   else if (m_downup == "up"){
		   // Server is set on the backbone node
		   UdpServerHelper udpServer (9);
		   ApplicationContainer udpServerApps = udpServer.Install (nc_bb);
		   udpServerApps.Start (Seconds (0.0));
		   udpServerApps.Stop (Seconds (m_totalTime));

		   // Client is set on all STAs
		   UdpClientHelper udpClient (if_p2p_gwBb.GetAddress  (0), 9);
		   udpClient.SetAttribute ("MaxPackets", UintegerValue ((uint32_t) (m_totalTime * (1 / m_packetInterval))));
		   udpClient.SetAttribute ("Interval", TimeValue (Seconds (m_packetInterval)));
		   udpClient.SetAttribute ("PacketSize", UintegerValue (m_packetSize));
		   ApplicationContainer udpClientApps = udpClient.Install (nc_sta);
		   udpClientApps.Start (Seconds (m_randomStart));
		   udpClientApps.Stop (Seconds (m_totalTime));
	   }
   }
 }

 int
 NokiaTest::Run ()
 {
   CreateNodes ();
   InstallInternetStack ();
   SetupMobility ();
   CreatePropagationLoss();
   InstallApplication ();

   //Gnuplot parameters
   std::string fileNameWithNoExtension = "FlowVSThroughput_ft_";
   std::string graphicsFileName = fileNameWithNoExtension + ".png";
   std::string plotFileName = fileNameWithNoExtension + ".plt";
   std::string plotTitle = "Flow vs Throughput";
   std::string dataTitle = "Throughput";

   // Open the data file
   DataFile.open("datafile.txt");


   // Instantiate the plot and set its title.
   Gnuplot gnuplot (graphicsFileName);
   gnuplot.SetTitle (plotTitle);

   // Make the graphics file, which the plot file will be when it
   // is used with Gnuplot, be a PNG file.
   gnuplot.SetTerminal ("png");

   // Set the labels for each axis.
   gnuplot.SetLegend ("Flow", "Throughput");


   Gnuplot2dDataset dataset;
   dataset.SetTitle (dataTitle);
   dataset.SetStyle (Gnuplot2dDataset::LINES_POINTS);

   //flowMonitor declaration
   FlowMonitorHelper fmHelper;
   Ptr<FlowMonitor> allMon = fmHelper.InstallAll ();
   // call the flow monitor function
   ThroughputMonitor (&fmHelper, allMon, dataset);


   Simulator::Stop (Seconds (m_totalTime));
   // Enable graphical interface for netanim
   AnimationInterface animation ("NokiaTest.xml");
   animation.EnablePacketMetadata (true);

   Simulator::Run ();
   //Gnuplot ...continued
   gnuplot.AddDataset (dataset);
   // Open the plot file.
   std::ofstream plotFile (plotFileName.c_str ());
   // Write the plot file.
   gnuplot.GenerateOutput (plotFile);
   // Close the plot file.
   plotFile.close ();
   // Close the data file
   DataFile.close();
   Simulator::Destroy ();

   return 0;
 }

 int
 main (int argc, char *argv[])
 {
   ns3::PacketMetadata::Enable ();
   NokiaTest t;
   t.Configure (argc, argv);
   return t.Run ();
 }

 void
 ThroughputMonitor (FlowMonitorHelper *fmhelper, Ptr<FlowMonitor> flowMon, Gnuplot2dDataset DataSet)
 {
   double localThrou = 0;
   double localper = 0;
   std::map<FlowId, FlowMonitor::FlowStats> flowStats = flowMon->GetFlowStats ();
   Ptr<Ipv4FlowClassifier> classing = DynamicCast<Ipv4FlowClassifier> (fmhelper->GetClassifier ());
   for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator stats = flowStats.begin (); stats != flowStats.end (); ++stats)
   {
	   // first 3 FlowIds are for ECHO apps, we don't want to display them
	   if (stats->first > 3)
	   {
		   Ipv4FlowClassifier::FiveTuple fiveTuple = classing->FindFlow (stats->first);
		   std::cout << "Flow ID     : " << stats->first << " ; " << fiveTuple.sourceAddress << " -----> " << fiveTuple.destinationAddress << std::endl;
		   std::cout << "Tx Packets = " << stats->second.txPackets << std::endl;
		   std::cout << "Rx Packets = " << stats->second.rxPackets << std::endl;
		   localper = 1-((float) stats->second.rxPackets/stats->second.txPackets);
		   std::cout << "PER: " << localper << std::endl;
		   std::cout << "Duration    : " << (stats->second.timeLastRxPacket.GetSeconds () - stats->second.timeFirstTxPacket.GetSeconds ()) << std::endl;
		   std::cout << "Last Received Packet  : " << stats->second.timeLastRxPacket.GetSeconds () << " Seconds" << std::endl;
		   std::cout << "Throughput: " << stats->second.rxBytes * 8.0 / (stats->second.timeLastRxPacket.GetSeconds () - stats->second.timeFirstTxPacket.GetSeconds ()) / 1000 / 1000 << " Mbps" << std::endl;
		   localThrou = (stats->second.rxBytes * 8.0 / (stats->second.timeLastRxPacket.GetSeconds () - stats->second.timeFirstTxPacket.GetSeconds ()) / 1000 / 1000);
		   // updata gnuplot data
		   DataSet.Add ((double) Simulator::Now ().GetSeconds (), (double) localThrou);
		   DataFile << "[" << (double) Simulator::Now ().GetSeconds ()<< "," << (double) localThrou << "," << (double) localper << "]," << std::endl;
		   std::cout << "---------------------------------------------------------------------------" << std::endl;
	   }
   }
   Simulator::Schedule (Seconds (1), &ThroughputMonitor, fmhelper, flowMon, DataSet);
   flowMon->SerializeToXmlFile ("infrastructure-mesh-backbone-throughputMonitor.xml", true, true);
 }
